package com.mycompany.asdasd;

import com.mycompany.asdasd.Cat;

public class Home {

    //lauks | field | поле
    private String someTop;

    private Cat cat;

    public void buildTop(long size) {
        long upSize = getPrice(size);
        System.out.println("Home->BuildTOP: "
                + upSize);
    }

    public void buildMiddle(long size) {
        someTop = "2";
        System.out.println("Home->buildMiddle: "
                + size);
    }

    protected void buildBottom(long size, long angle) {
        someTop = "3";
        System.out.println("Home->buildBottom: "
                + size + " " + angle);
    }

    //long Long
    private long getPrice(long param) {
        return param + 10;
    }

    public String getSomeTop() {
        return someTop;
    }

    public void setSomeTop(String someTop) {
        this.someTop = someTop;
    }

    public Cat getCat() {
        return cat;
    }

    public void setCat(Cat cat) {
        this.cat = cat;
    }

    @Override
    public String toString() {
        return "Home{" +
                "someTop='" + someTop + '\'' +
                ", cat=" + cat +
                '}';
    }
}
