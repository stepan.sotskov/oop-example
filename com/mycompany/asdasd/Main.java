package com.mycompany.asdasd;

import com.mycompany.asdasd.CrismasTree;
import com.mycompany.asdasd.DNA;
import com.mycompany.asdasd.GreenFlower;

public class Main {

    public static void main(String[] args) {

        DNA flower = new GreenFlower();
        DNA crismasTree = new CrismasTree();
        something(flower);
        something(crismasTree);
    }

    public static void something(DNA dna){
        dna.eat();
        dna.breath();
    }
}
