package com.mycompany.asdasd;

import com.mycompany.asdasd.Home;

public class Home2 extends Home {

    private int size2;
    @Override
    public void buildMiddle(long size) {
        System.out.println("HomeTwo->buildMiddleTwo: "
                + size);
    }

    @Override
    protected void buildBottom(long size, long angle) {
        System.out.println("HomeTwo->buildBottomTwo: "
                + size + " " + angle);
    }

    @Override
    public void buildTop(long size) {
        System.out.println("HomeTwo->buildTopTwo: "
                + size);
    }

      //ovverload
    public void buildTop(long size, long size2) {
        buildTop(size);
        System.out.println("Additional Functionallity: " + size2);
        super.buildTop(size);
    }


}
