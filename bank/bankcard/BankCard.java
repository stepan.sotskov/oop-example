package bank.bankcard;

public interface BankCard {

    void putMoney(float amount);
    void removeMoney(float amount);

}
