package bank.bankcard;

import bank.AbstractCard;
import bank.BankAccount;

public class CreditCard extends AbstractCard {

    private float applyOperation(float charge){ // komissija + nonemta nauda
        return charge + 0.50f;
    }

    @Override
    public void removeMoney(float amount) {

        float tempAmount;

        BankAccount tempAcc = getBankAccount();
        tempAmount = tempAcc.getAmount();//vai  getBankAccount.getAmount();
        tempAmount = tempAmount - applyOperation(amount); // nonem komisiju 50 c
        tempAcc.setAmount(tempAmount);


    }
}
