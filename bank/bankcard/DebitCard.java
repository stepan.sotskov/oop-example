package bank.bankcard;

import bank.AbstractCard;
import bank.BankAccount;

public class DebitCard extends AbstractCard {

    @Override
    public void removeMoney(float amount) {

        float tempAmount;

        BankAccount tempAcc = getBankAccount();
        tempAcc.getAmount();

        tempAmount = tempAcc.getAmount();//getBankAccount.getAmount();
        tempAmount = tempAmount - amount;

        if (tempAmount > 0) {
            tempAcc.setAmount(tempAmount);
        } else {
            throw new RuntimeException("Amount for charge too big " + tempAmount);
        }


    }


}
