package bank;

import bank.bankcard.CreditCard;
import bank.bankcard.DebitCard;

public class Main {

    public static void main(String[] args) {

        BankAccount bankAccount = new BankAccount();
        bankAccount.setOwner("Andris");
        bankAccount.setNum("12345678");
        bankAccount.setAmount(1000);
        System.out.println(bankAccount);

        CreditCard creditCard = new CreditCard();
        creditCard.setCardNum("123123");
        creditCard.setHolder("Alex");
        creditCard.setBankAccount(bankAccount);
        creditCard.putMoney(1000);
        System.out.println();
        creditCard.removeMoney(200);
        System.out.println(bankAccount);

        DebitCard debitCard = new DebitCard();
        debitCard.setCardNum("321321");
        debitCard.setHolder("Māra");
        debitCard.setBankAccount(bankAccount);
        debitCard.putMoney(90);
        debitCard.removeMoney(30);
        System.out.println(bankAccount);

    }


}
