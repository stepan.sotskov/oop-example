package bank;

public abstract class AbstractCard implements bank.bankcard.BankCard {


    private String holder;
    private String cardNum;
    private BankAccount bankAccount; // Compozicija reference



    @Override
    public void putMoney(float amount){
        float tempAmount;
        tempAmount = bankAccount.getAmount();
        tempAmount = tempAmount + amount;
        bankAccount.setAmount(tempAmount);
    }

    public String getHolder() {
        return holder;
    }

    public void setHolder(String holder) {
        this.holder = holder;
    }

    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }
}
